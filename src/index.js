import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  KeyboardAvoidingView,
  Modal,
  TouchableOpacity,
  Animated,
  PanResponder,
  Platform,
  Image,
  Dimensions,
  Text
} from "react-native";
import styles from "./style";
import Video from 'react-native-video';
import
  MediaControls, {PLAYER_STATES}
from './MediaControl/index';


const SUPPORTED_ORIENTATIONS = [
  "portrait",
  "portrait-upside-down",
  "landscape",
  "landscape-left",
  "landscape-right"
];

class RBSheet extends Component {
  videoPlayer;

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      animatedHeight: new Animated.Value(0),
      animatedVHeight: new Animated.Value(0),
      pan: new Animated.ValueXY(),
      videoUp:true,
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: true,
      playerState: PLAYER_STATES.PLAYING
    };

    this.createPanResponder(props);
  }

  setModalVisible(visible, props) {
    const { height, minClosingHeight, openDuration, closeDuration, onClose, onOpen } = this.props;
    const { animatedHeight,animatedVHeight, pan } = this.state;
    if (visible) {
      this.setState({ modalVisible: visible });
      if (typeof onOpen === "function") onOpen(props);
      Animated.parallel([
      Animated.timing(animatedHeight, {
        useNativeDriver: false,
        toValue: height,
        duration: openDuration
      }),
      Animated.timing(animatedVHeight, {
        delay:openDuration,
        useNativeDriver: false,
        toValue: 30,
        duration: 0
      }),
    ]).start();
    } else {
      this.setState({ videoUp: true });
      Animated.parallel([
        Animated.timing(animatedHeight, {
          useNativeDriver: false,
          toValue: minClosingHeight,
          duration: closeDuration
        }),
        Animated.timing(animatedVHeight, {
          useNativeDriver: false,
          toValue: 0,
          duration: 0
        }),
      ]).start(() => {
        pan.setValue({ x: 0, y: 0 });
        this.setState({
          modalVisible: visible,
          animatedHeight: new Animated.Value(0),
          animatedVHeight: new Animated.Value(0),
        });

        if (typeof onClose === "function") onClose(props);
      });
    }
  }

  createPanResponder(props) {
    const { closeOnDragDown, height } = props;
    const { pan } = this.state;
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => closeOnDragDown,
      onPanResponderMove: (e, gestureState) => {
        if (gestureState.dy > 0) {
          Animated.event([null, { dy: pan.y }], { useNativeDriver: false })(e, gestureState);
        }
      },
      onPanResponderRelease: (e, gestureState) => {
        if (height / 4 - gestureState.dy < 0) {
          this.setModalVisible(false);
        } else {
          Animated.spring(pan, { toValue: { x: 0, y: 0 }, useNativeDriver: false }).start();
        }
      }
    });
  }

  open(props) {
    this.setModalVisible(true, props);
  }

  close(props) {
    this.setModalVisible(false, props);
  }


  onSeek = seek => {
    this.videoPlayer.seek(seek);
  };

  onPaused = playerState => {
    this.setState({
      paused: !this.state.paused,
      playerState
    });
  };

  onReplay = () => {
    this.setState({ playerState: PLAYER_STATES.PLAYING,paused:true });
    this.videoPlayer.seek(0);
  };

  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };

  onLoad = data => this.setState({ duration: data.duration, isLoading: false });

  onLoadStart = data => this.setState({ isLoading: true });

  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });

  onError = () => alert("Oh! ", error);

  exitFullScreen = () => {};

  enterFullScreen = () => {};

  onFullScreen = () => {};

  render() {
    const {
      animationType,
      closeOnDragDown,
      dragFromTopOnly,
      closeOnPressMask,
      closeOnPressBack,
      children,
      customStyles,
      keyboardAvoidingViewEnabled,
      onVideo,
      videoUrl,
      showCredentials,
      showCredentialsContainer
    } = this.props;
    const { animatedHeight, pan, modalVisible,videoUp,animatedVHeight } = this.state;
    const panStyle = {
      transform: pan.getTranslateTransform()
    };
    const height = Math.ceil(Dimensions.get('window').height);
    const animatedVideoHeight = new Animated.Value(height > 700? height > 750 ? height * 0.18: height * 0.17:height * 0.17);
    const imgRotateStartVal = new Animated.Value(0);

    const animatedVideoHeightDown = new Animated.Value(0);
    const imgRotateStartValDown = new Animated.Value(0);

   const slideup=()=>{
    Animated.parallel([
      Animated.timing(animatedVideoHeight, {
        useNativeDriver: false,
        toValue: 0,
        duration: 500
      }),
      Animated.timing(imgRotateStartVal, {
        delay: 100,
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      })
    ]).start(()=>{this.setState({
      videoUp:false
    })
    });
    }

   const slidedown=()=>{
      Animated.parallel([
        Animated.timing(animatedVideoHeightDown, {
          useNativeDriver: false,
          toValue: height > 700? height > 750 ? height * 0.18: height * 0.17:height * 0.17,
          duration: 500
        }),
        Animated.timing(imgRotateStartValDown, {
          delay: 100,
          toValue: 1,
          duration: 500,
          useNativeDriver: true,
        })
      ]).start(()=>{this.setState({
        videoUp:true,
        paused:true,
      })
      });
    }
    const rotateInterpolateLeft = imgRotateStartVal.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
   const rotateInterpolateLeftDown = imgRotateStartValDown.interpolate({
      inputRange: [0, 1],
      outputRange: ['180deg', '0deg'],
    });
    return (
      <Modal
        transparent
        animationType={animationType}
        visible={modalVisible}
        supportedOrientations={SUPPORTED_ORIENTATIONS}
        onRequestClose={() => {
          if (closeOnPressBack) this.setModalVisible(false);
        }}
        statusBarTranslucent
        backdropColor="transparent"
      >
        <KeyboardAvoidingView
          enabled={keyboardAvoidingViewEnabled}
          behavior="padding"
          style={[styles.wrapper, customStyles.wrapper]}
        >
          <TouchableOpacity
            style={styles.mask}
            activeOpacity={1}
            onPress={() => (closeOnPressMask ? this.close() : null)}
          />
      {
        showCredentials === true &&
          showCredentialsContainer
      }


        {
          (onVideo === true && animatedVHeight.__getValue()===30) && (
          <Animated.View
            {...(!dragFromTopOnly && this.panResponder.panHandlers)}
            style={[panStyle,{position:'absolute',width:'100%',height:'30%',backgroundColor:'#000000',top : videoUp?animatedVideoHeight:animatedVideoHeightDown,flex:1}]}
          >
            <TouchableOpacity style={{width:35,height:35,left:'90%',zIndex: 1,}} onPress={videoUp?slideup:slidedown} activeOpacity={0.8}>
            <Animated.Image style={[{width:35,height:35},{transform: [{rotate:videoUp?rotateInterpolateLeft:rotateInterpolateLeftDown}]}]} source={require('./assets/up-arrow.png')}/>
            </TouchableOpacity>
            <Video
              source={{uri:videoUrl}}
              ref={videoPlayer => (this.videoPlayer = videoPlayer)}
              resizeMode="cover"
              fullscreen={false}
              onBuffer={this.onBuffer}
              onError={this.videoError}
              style={styles.mediaPlayer}
              onEnd={this.onEnd}
              onLoad={this.onLoad}
              paused={this.state.paused}
              onProgress={this.onProgress}
              onLoadStart={this.onLoadStart}
              repeat={true}
              posterResizeMode="cover"
              />
            <MediaControls
              onSeek={this.onSeek}
              onReplay={this.onReplay}
              onPaused={this.onPaused}
              onSeeking={this.onSeeking}
              duration={this.state.duration}
              isLoading={this.state.isLoading}
              onFullScreen={this.onFullScreen}
              progress={this.state.currentTime}
              playerState={this.state.playerState}
              showOnStart={true}
            />
          </Animated.View>)
        }

          <Animated.View
            {...(!dragFromTopOnly && this.panResponder.panHandlers)}
            style={[panStyle, styles.container, { height: animatedHeight },{zIndex:20}, customStyles.container]}
          >
            {closeOnDragDown && (
              <View
                {...(dragFromTopOnly && this.panResponder.panHandlers)}
                style={[styles.draggableContainer, customStyles.draggableContainer]}
              >
                <View style={[styles.draggableIcon, customStyles.draggableIcon]} />
              </View>
            )}
            {children}
          </Animated.View>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

RBSheet.propTypes = {
  animationType: PropTypes.oneOf(["none", "slide", "fade"]),
  height: PropTypes.number,
  minClosingHeight: PropTypes.number,
  openDuration: PropTypes.number,
  closeDuration: PropTypes.number,
  closeOnDragDown: PropTypes.bool,
  closeOnPressMask: PropTypes.bool,
  dragFromTopOnly: PropTypes.bool,
  closeOnPressBack: PropTypes.bool,
  showCredentials:PropTypes.bool,
  showCredImage:PropTypes.any,
  onVideo:PropTypes.bool,
  videoUrl:PropTypes.string,
  keyboardAvoidingViewEnabled: PropTypes.bool,
  customStyles: PropTypes.objectOf(PropTypes.object),
  onClose: PropTypes.func,
  onOpen: PropTypes.func,
  children: PropTypes.node
};

RBSheet.defaultProps = {
  animationType: "none",
  height: 260,
  minClosingHeight: 0,
  openDuration: 300,
  closeDuration: 200,
  closeOnDragDown: false,
  dragFromTopOnly: false,
  closeOnPressMask: true,
  closeOnPressBack: true,
  showCredentials:false,
  showCredImage:'',
  onVideo:false,
  videoUrl:'',
  keyboardAvoidingViewEnabled: Platform.OS === "ios",
  customStyles: {},
  onClose: null,
  onOpen: null,
  children: <View />
};

export default RBSheet;
